import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HomeComponent } from './views/home/home.component';
import { TraductionComponent } from './views/traduction/traduction.component';
import { NavComponent } from './views/nav/nav.component';
import { PostsComponent } from './views/posts/posts.component';

/* Configuración de ngx translate con AOT, es necesario verificar la habilitación de este mismo*/
export function createTranslateLoader(http: HttpClient) {
  /* Determinamos una ruta en donde manejaremos los .json para la traducción, cabe mencionar
     que esto varia según el método que usemos existen 3 distintos.
     Tomarlo como un .Json
     Tomarlo como un .fa
     Transformar el archivo .po a http */
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TraductionComponent,
    NavComponent,
    PostsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (createTranslateLoader),
          deps: [HttpClient]
      }
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
