import { Component } from '@angular/core';
/* Hacemos uso del servicio, se anexa en este componente debido a que es el primero en ejecutarse */
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  langs: string[] = [];
  /**
   * Llamamos al servicio de ngx translate
   * @param translate hace uso de los métodos definidos en el servicio del modulo importado
   */
  constructor(
    private translate: TranslateService
  ){
    /* Inicializamos por defecto el idioma como inglés*/
    this.translate.setDefaultLang('en');
    /* Al utilizar .use sobreescribimos el idioma por defecto, en este caso nos dice que el idioma definido
    es el inglés */
    this.translate.use('es');
    /* Hacemos uso de addLangs que contiene los idiomas que deseamos utilizar, en este caso Español e Inglés */
    this.translate.addLangs(['es','en']);
    /* En el arreglo de string, obtenemos los d */
    this.langs = this.translate.getLangs();

    this.translate.stream('HELLO')
    .subscribe((res: string) =>{
      console.log(res);
    });

    this.translate.stream('GREETING', {name: 'francisco'})
    .subscribe((res: string) =>{
      console.log(res);
    });
  }

  changeLang(lang: string){
    this.translate.use(lang);
  }

  showAlert(){
    const msg = this.translate.instant('Error 404');
    alert(msg);
  }

  title = 'curso-sass';
}
