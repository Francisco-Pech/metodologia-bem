import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.sass']
})
export class PostsComponent implements OnInit {

  allCharacter: [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    const lang = localStorage.getItem('lang') || 'es';

    console.log(lang);
    const headers = new HttpHeaders({
      'Accept-Language': lang
    });

    this.http.get("https://rickandmortyapi.com/api/character",{ headers: headers}).subscribe((response:any) =>{
      this.allCharacter = response.results;
      console.log(this.allCharacter);
    }, (error) => {
      alert("El error es: " + error.statusText);
    });
  }


  /*
  
  this.RickMortinServices.getCharacter().subscribe((response: any)=>{
      console.log(response);
      this.allCharacter =  response.results;
    }, (error) => {
      alert("El error es: " + error.statusText);
    });*/
}
