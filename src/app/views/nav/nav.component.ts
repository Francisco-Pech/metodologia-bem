import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.sass']
})
export class NavComponent implements OnInit {

  lang;

  constructor() { 
    this.lang = localStorage.getItem('lang') || 'es';
  }

  ngOnInit(): void {
  }

  changeLang = (lang) =>{
    localStorage.setItem('lang',lang);
    window.location.reload();
  }

}
